# mk_venv     

Create a python 3 virtual environment (venv) in a directory called `p3`.

# Usage:

## To create the venv

```
cd $GITSANDBOX
mk_venv 
```

## To enable the venv

```
cd $GITSANDBOX
source p3/bin/activate
```

## To Disable the venv

```
deactivate
```

# Process

The script automates the three following steps

- Remove any existing `p3` directory
- Create a new venv in `p3`
- Activate the new venv
- Update `pip` itself
- Install everything in the `requirements.txt` if one exists

# Links

- https://docs.python.org/3/tutorial/venv.html
- https://packaging.python.org/guides/tool-recommendations/
- https://packaging.python.org/key_projects/



